name: SNT02 - Évaluation
topic: SC.NUMERIQ.TECHNOL.
level: 2NDE

questions:
  #--------------------------------------------------
  # Histoire du Web
  #--------------------------------------------------
  - type: match
    name: Histoire du Web
    level: 1
    text: Reconstruire la chronologie de création et d'évolution du Web en associant les événements aux bonnes dates.
    answers:
      - 1965|Concept d'hypertexte
      - 1989|Invention du Web
      - 1993|Passage dans le domaine public
      - 1995|Web dynamique (PHP/Javascript)
      - 2001|Standardisation (DOM)
      - 2010|Développement sur mobile

  - type: single
    name: Histoire du Web
    level: 1
    text: Qui a défini les principes de ce qui deviendra la Web ?
    answers:
      - L'armée américaine
      - Ted Nelson
      - Tim Berners Lee (OK)
      - Google

  - type: single
    name: Histoire du Web
    level: 1
    text: Quel genre d'application faut-il pour accéder au Web ?
    answers:
      - Un moteur de recherche
      - Un navigateur web (OK)
      - Un crawler web
      - Un client WIFI

  - type: single
    name: Histoire du Web
    level: 2
    text: Quelle est la particularité du navigateur web Mosaic ?
    answers:
      - C'est le premier navigateur web
      - C'est le premier à gérer la vidéo
      - C'est le premier à gérer les images (OK)
      - C'est le premier à gérer le langage Javascript

  - type: single
    name: Histoire du Web
    level: 2
    text: Quelle est la contribution de Ted Nelson dans la création du Web ?
    answers:
      - Le protocole HTTP
      - Le concept d'hypertexte (OK)
      - Le premier navigateur Web
      - Le premier moteur de recherche

  - type: multiple
    name: Histoire du Web
    level: 3
    text: Parmi les propositions suivantes, lesquelles correspondent aux principes du Web définis dans les années 80 ?
    answers:
      - Le langage HTML et l'hypertexte (OK)
      - Les moteurs de recherche
      - Le couple client web et serveur web (OK)
      - Le protocole HTTP (OK)
      - Le protocole TCP/IP




  #--------------------------------------------------
  # Les adresses web
  #--------------------------------------------------
  - type: single
    name: Les adresses web
    level: 1
    text: |
      Le sigle URL pour <em>Uniform Ressource Locator</em> désigne :
    answers:
      - Un protocole de communication concurrent de HTTP
      - L'adresse d'une machine sur le réseau Internet
      - L'adresse d'une ressource sur le Web (OK)
      - Un synonyme de lien hypertexte

  - type: single
    name: Les adresses web
    level: 2
    text: Parmi ces adresses web, laquelle est correctement écrite ?
    answer_format: code
    answers:
      - https://lemonde/accueil
      - https:\\www.lemonde.fr\accueil
      - http://www.lemonde.fr/accueil (OK)
      - html://www.lemonde.fr/accueil

  - type: short
    name: Les adresses web
    level: 2
    grade: 2
    text: >
      Quel est le chemin de l'URL suivante : 
      <span style="font-family: 'courier new', courier;">https://www.onisep.fr/outils/parcoursup?id=1</span>
    answers:
      - /outils/parcoursup

  - type: short
    name: Les adresses web
    level: 2
    grade: 2
    text: >
      Quel est le nom de domaine de l'URL suivante : 
      <span style="font-family: 'courier new', courier;">http://www.bachelard.fr/cantine/menu.html</span>
    answers:
      - www.bachelard.fr

  - type: short
    name: Les adresses web
    level: 2
    grade: 1
    text: >
      Quel est le protocole de l'URL suivante : 
      <span style="font-family: 'courier new', courier;">ftp://www.filestore.fr/download</span>
    answers:
      - "ftp"
      - "ftp:"
      - "ftp://"
      - "ftp ://"
      - "ftp : //"
      - "ftp: //"



  #--------------------------------------------------
  # L'infrastructure du Web
  #--------------------------------------------------
  - type: single
    name: L'infrastructure du Web
    level: 1
    text: Parmi les propositions suivantes, laquelle correspond à un client web ?
    answers:
      - L'ordinateur
      - La box Internet
      - Le navigateur web (OK)
      - Le site web
      - L'internaute

  - type: cloze
    name: L'infrastructure du Web
    title: Accès à un site web
    level: 1
    text: |      
      Pour consulter une ressource web, il faut utiliser {Internet|un moteur de recherche|un navigateur web (OK)|un ordinateur}.<br />
      Il suffit ensuite d'écrire {le titre|l'URL (OK)|les mots-clés} de la ressource dans la barre d'adresse.<br />
      Le nom de domaine est alors transformé en une adresse IP permettant la transmission d'une requête {TCP|IP|HTTP (OK)|DNS|HTML}  
      au {client web|serveur web (OK)|moteur de recherche|réseau} hébergeant la ressource.<br />
      Ce dernier traite la requête et envoie en retour une réponse {TCP|HTTP (OK)|HTML|CSS} avec le contenu demandé.<br />
      Si le contenu n'existe pas, le serveur envoie une réponse avec comme code d'erreur {200|301|404 (OK)|500|800}
      
  - type: single
    name: L'infrastructure du Web
    level: 2
    text: Quelle est la particularité du modèle client-serveur du Web ?
    answers:
      - Il s'agit d'une communication entre deux machines
      - Il s'agit d'une communication entre deux applications (OK)
      - Il s'agit d'une communication pair-à-pair
      - Il s'agit d'une communication systématiquement chiffrée

  - type: single
    name: L'infrastructure du Web
    level: 3
    text: > 
      Vous avez réalisé une page web sur votre ordinateur. 
      Ce dernier est accessible depuis l'extérieur via Internet. Malgré cela, votre page reste inaccessible des internautes.
      Que reste-t-il à faire pour régler ce problème ?
    answers:
      - Installer un client web
      - Installer un serveur web (OK)
      - Créer une feuille de style
      - Convertir votre page en PHP




  #--------------------------------------------------
  # Les protocoles du Web
  #--------------------------------------------------
  - type: single
    name: Les protocoles du Web
    level: 1
    text: Que signifie l'acronyme HTTP ?
    answers:
      - HyperText Transfer Protocol (OK)
      - HTml Transport Protocol
      - HyperText Transfer Procedure
      - High Text Transport Protocol

  - type: single
    name: Les protocoles du Web
    level: 1
    text: Que signifie le <strong>S</strong> de HTTP<strong>S</strong> ?
    answers:
      - Service
      - Secret
      - Secure (OK)
      - Server
      - Strong

  - type: multiple
    name: Les protocoles du Web
    level: 2
    text: À quoi voit-on qu'une connexion à un site web est sécurisée ?
    answers:
      - À la présence d'un cadenas (OK)
      - À la présence d'une fenêtre "Politique cookies"
      - À la présence d'une URL commençant par https:// (OK)
      - À la présence du terme "secure" dans l'URL

  - type: single
    name: Les protocoles du Web
    level: 2
    text: Que représente l'extrait de texte ci-dessous ?
    image: http_request.png
    answers:
      - Une requête TCP
      - Une requête Web
      - Une requête HTTP (OK)
      - Une requête HTTPS
      - Une requête HTML

  - type: single
    name: Les protocoles du Web
    level: 3
    text: Que signifie la présence du cadenas et l'usage du protocole HTTPS lorsqu'on accède à un site web ?
    answers:
      - Le site web respecte la vie privée (pas de cookies)
      - Le site web est certifié sans virus
      - Les données échangées entre le navigateur web et le site sont chiffrées (OK)
      - Les informations présentes sur le site sont fiables
      - Le site web est payant

  - type: single
    name: Les protocoles du Web
    level: 3
    text: Quelle est le nature des données transmises via la réponse HTTP ci-dessous ?
    image: http_response.png
    answers:
      - Une image
      - Une feuille de style (OK)
      - Un document HTML
      - Un document Javascript




  #--------------------------------------------------
  # Les langages du Web
  #--------------------------------------------------
  - type: single
    name: Les langages du Web
    level: 1
    text: Quel langage informatique faut-il utiliser pour réaliser une page web ?
    answers:
      - Notepad++
      - HTML (OK)
      - Chrome
      - HTTP
      - L'anglais

  - type: multiple
    name: Les langages du Web
    level: 1
    text: Sur une page web, à quoi sert le code HTML ?
    answers:
      - À définir le contenu de la page (OK)
      - À définir la structure de la page (OK)
      - À définir les éléments interactifs de la page
      - À définir le style de la page

  - type: single
    name: Les langages du Web
    level: 2
    text: Quel langage informatique permet la mise en forme d'une page web ?
    answers:
      - CSS (OK)
      - HTML
      - Javascript
      - PHP

  - type: single
    name: Les langages du Web
    level: 2
    text: Quelle balise permet la création d'un lien hypertexte ?
    answer_format: code
    answers:
      - <a> (OK)
      - <link>
      - <em>
      - <href>

  - type: single
    name: Les langages du Web
    level: 3
    text: >
      Nous souhaitons créer un lien hypertexte vers le site de Wikipédia. 
      Parmi les propositions suivantes, laquelle est correcte ?
    answer_format: code
    answers:
      - <a href="Wikipédia">https://www.wikipedia.fr</a>
      - <a href="https://www.wikipedia.fr">Wikipédia</a> (OK)
      - <a url="https://www.wikipedia.fr">Wikipédia</a>
      - <a href="https://www.wikipedia.fr" title="Wikipédia" />

  - type: single
    name: Les langages du Web
    level: 3
    text: Quel sélecteur CSS utiliser pour mettre en forme <strong>uniquement</strong> le texte <em>"Titre principal"</em> ?
    image: html.png
    answer_format: code
    answers:
      - .h1
      - h1 (OK)
      - body
      - title

  - type: single
    name: Les langages du Web
    level: 3
    text: Quel sélecteur CSS utiliser pour mettre en forme <strong>uniquement</strong> le texte <em>"Bonjour !"</em> ?
    image: html.png
    answer_format: code
    answers:
      - p
      - .p
      - .intro (OK)
      - body

  - type: single
    name: Les langages du Web
    level: 3
    text: Quelle balise permet de définir le titre de l'onglet dans le navigateur web ?
    image: html.png
    answer_format: code
    answers:
      - <title> (OK)
      - <h1>
      - <head>
      - <p>




  #--------------------------------------------------
  # Les moteurs de recherche
  #--------------------------------------------------
  - type: multiple
    name: Les moteurs de recherche
    level: 1
    text: Parmi les propositions suivantes, lesquelles correspondent à un moteur de recherche ?
    answers:
      - Qwant (OK)
      - Chrome
      - Firefox
      - Google (OK)
      - Bing (OK)
      - Windows

  - type: single
    name: Les moteurs de recherche
    level: 1
    text: > 
      Comment un moteur de recherche est-il informé de l'existence d'un site web ?<br />
      <em>(penser aux phases de fonctionnement d'un moteur de recherche)</em>
    answers:
      - En contactant très régulièrement les serveurs DNS
      - En explorant très régulièrement l'ensemble du Web (OK)
      - En explorant très rapidement le Web pour chaque requête recherche
      - Grâce aux créateurs du site qui alertent le moteur de recherche
      - Grâce aux internautes qui signalent tout nouveau site au moteur de recherche

  - type: single
    name: Les moteurs de recherche
    level: 1
    text: Comment se nomme le traitement consistant à extraire les mots d'une page web et à les associer à une ou plusieurs pages web ?
    answers:
      - L'exploration
      - L'indexation (OK)
      - Le classement
      - Le référencement

  - type: single
    name: Les moteurs de recherche
    level: 1
    text: Sur quoi se base la popularité d'une page ?
    answers:
      - La fréquence d'apparition de certains mots-clés
      - Le nombre total de mots par page
      - Le nombre de liens entrants sur cette page (OK)
      - Le nombre de liens sortants de cette page

  - type: single
    name: Les moteurs de recherche
    level: 2
    text: Quel moteur de recherche recommanderiez-vous à quelqu'un qui ne souhaite pas l'exploitation de ses données personnelles ?
    answers:
      - Google
      - Bing
      - Qwant (OK)
      - Ecosia

  - type: single
    name: Les moteurs de recherche
    level: 2
    text: À quelle fréquence les robots d'indexation visitent-ils une page web ?
    answers:
      - Jusqu'à plusieurs fois par semaine (OK)
      - Environ une fois par mois
      - Environ une fois par an
      - Une seule fois, au moment de la découverte de la page




#--------------------------------------------------
# La confidentialité
#--------------------------------------------------
  - type: single
    name: La confidentialité
    level: 1
    text: Qu'est-ce qu'un cookie ?
    answers:
      - C'est une information envoyée par le navigateur web et stockée sur un serveur web
      - C'est une information envoyée par un serveur web et stockée sur le navigateur web (OK)
      - C'est une information stockée dans les pages web
      - C'est un protocole du Web

  - type: multiple
    name: La confidentialité
    level: 2
    text: Quelles précautions doit-on prendre lorsqu'on utilise un ordinateur public ou l'ordinateur d'un tiers ?
    answers:
      - Utiliser la navigation privée (OK)
#     - Refuser l'utilisation des cookies
      - Utiliser un VPN
      - Redémarrer l'ordinateur ou le téléphone
      - Effacer l'intégralité des données de navigation (OK)
      - Baisser la luminosité de l'écran

  - type: multiple
    name: La confidentialité
    level: 2
    text: Quels sont les usages possibles des cookies ?
    answers:
      - Stockage de votre état de connexion à un service en ligne (OK)
      - Stockage de votre historique de navigation
      - Publicité ciblée à partir d'informations stockées via les cookies (OK)
      - Stockage de votre adresse IP

  - type: single
    name: La confidentialité
    level: 2
    text: Que permet la navigation privée ?
    answers:
      - De naviguer de façon anonyme sur le Web en masquant l'adresse IP
      - De supprimer toutes les données de navigation à la fermeture du navigateur (OK)
      - D'avoir le statut "déconnecté" lorsqu'on consulte les réseaux sociaux
      - De chiffrer la connexion et ainsi les échanges privés




#--------------------------------------------------
# Le droit d'auteur
#--------------------------------------------------
  - type: single
    name: Le droit d'auteur
    level: 1
    text: Partager une œuvre sur le Web revient-il à la rendre libre de droits et utilisable par tous ?
    answers:
      - Oui
      - Non (OK)

  - type: single
    name: Le droit d'auteur
    level: 1
    text: Qu'est-ce que le droit moral ?
    answers:
      - C'est le droit qui lie un artiste à son œuvre (OK)
      - C'est le droit qui permet à un artiste d'exploiter son œuvre
      - C'est le droit qui encadre ce qu'est une œuvre, afin qu'elle respecte la morale
      - C'est le droit qui encadre les usages possibles d'une œuvre

  - type: single
    name: Le droit d'auteur
    level: 1
    text: Qu'est-ce que les droits patrimoniaux ?
    answers:
      - Ce sont les droits qui lie un artiste à son œuvre
      - Ce sont les droits qui permettent à un artiste d'exploiter son œuvre (OK)
      - Ce sont les droits qui permettent aux enfants d'un artiste d'hériter de son œuvre
      - Ce sont les droits qui permettent à un artiste d'intégrer son œuvre au patrimoine national

  - type: multiple
    name: Le droit d'auteur
    level: 2
    text: Le droit moral est le droit qui lie un artiste à son œuvre. Quelles sont ses spécificités ?
    answers:
      - Il est inaliénable (OK)
      - Il peut être cédé
      - Il est perpétuel (OK)
      - Il est limité dans le temps
      - Il ne s'applique qu'à certaines œuvres

  - type: multiple
    name: Le droit d'auteur
    level: 2
    text: |
      Les droits patrimoniaux sont les droits qui permettent à un artiste d'exploiter son œuvre.
      Ce sont ces droits qui vous empêchent d'utiliser librement un œuvre (image, musique, vidéo, ...) téléchargée depuis le Web.
      <br /><br />
      Dans quelles situations vous est-il cependant possible d'utiliser une œuvre ?
    answers:
      - Lorsque le nom de l'auteur est cité
      - Lorsque l'œuvre est sous licence libre (OK)
      - Lorsque l'auteur de l'œuvre est décédé depuis au moins 70 ans (OK)
      - Lorsqu'une nouvelle œuvre est créée à partir de l'originale (remix d'une musique par exemple)