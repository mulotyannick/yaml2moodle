Regarder le fichier `pyproject.toml` pour les bibliothèques Python à installer.

Si Poetry disponible, exemple :

```
poetry update
poetry shell
cd src
python main.py ../test.yaml
```