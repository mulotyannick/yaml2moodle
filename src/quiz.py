import xml.etree.ElementTree as ET
import os
import base64
from helpers import format_div_tags, format_code_tags, format_span_tags

from yaml import load

try:
    from yaml import CLoader as Loader, CDumper as Dumper
except ImportError:
    from yaml import Loader, Dumper


class Quiz:
    def __init__(self, name, topic="", level=""):
        self.name = name
        self.topic = topic
        self.level = level
        self._questions = []

    def add(self, question):
        self._questions.append(question)

    def _xml_category(self):
        question = ET.Element("question", type="category")
        category = ET.SubElement(question, "category")
        ET.SubElement(category, "text").text = self._xml_pronote_label()
        return question

    def _xml_pronote_label(self):
        infos = ET.Element("infos")
        ET.SubElement(infos, "name").text = self.name
        ET.SubElement(infos, "answernumbering").text = "123"
        ET.SubElement(infos, "niveau").text = self.level
        ET.SubElement(infos, "matiere").text = self.topic
        return ET.tostring(infos).decode("utf-8")

    def to_xml(self):
        root = ET.Element("quiz")
        root.append(self._xml_category())
        for question in self._questions:
            root.append(question.to_xml())

        return root

    @staticmethod
    def from_yaml(f):
        data = load(f, Loader=Loader)
        asset_path = os.path.dirname(f.name)
        quiz = Quiz(data["name"], data["topic"], data["level"])

        for question_data in data["questions"]:
            cls = _TYPES[question_data["type"]]
            quiz.add(cls(question_data, asset_path))

        return quiz

    def write(self, f):
        xml = self.to_xml()
        ET.ElementTree(xml).write(f, encoding='utf-8', method='xml')

    def print_stats(self):
        question_count = len(self._questions)

        question_points = 0
        question_grades = {}
        question_time = 0

        for question in self._questions:
            question_points += question.grade
            question_time += question.estimated_time()

            grade_key = question.grade
            if grade_key not in question_grades:
                question_grades[grade_key] = 1
            else:
                question_grades[grade_key] += 1

        question_time = round(question_time / 60)

        print(f"Nombre de questions  : {question_count}")
        for grade, count in question_grades.items():
            s = "s" if grade > 1 else " "  # "s" éventuel au mot point
            t = grade * count  # nombre total de points
            p = round((count * 100) / question_count)  # Pourcentage de la note de l'évaluation
            pp = round(20 * p / 100, 2)  # Nombre de points sur 20
            print(f"Questions à {grade} point{s} : {count} ({t} points / {p} % / {pp})")
        print(f"Total de points      : {question_points}")
        print(f"Temps estimé         : {question_time} minutes")


class Question:
    def __init__(self, data=None, asset_path="."):
        self.name = self._value(data, "name")
        self.text = self._value(data, "text")
        self._image_b64 = None
        self.grade = int(self._value(data, "grade", "0"))
        self._type = ""
        self._attributes = {}
        self._case_sensitive = True
        self._tags = []
        self._answers = []
        self._asset_path = asset_path
        self.set_required(True)

        if data and "level" in data:
            self.set_level(int(data["level"]))

        if data and "image" in data:
            self.set_image(data['image'])

    @staticmethod
    def _value(data, field, default=""):
        return data[field] if field in data else default

    def add_answer(self, value, validation=False, feedback=""):
        self._answers.append((value, validation, feedback))

    @property
    def answers(self):
        return self._answers

    def estimated_time(self):
        return 0

    def add_tag(self, tag):
        if tag not in self._tags:
            self._tags.append(tag)

    def remove_tag(self, tag):
        if tag in self._tags:
            self._tags.remove(tag)

    def set_level(self, level):
        assert 1 <= level <= 3
        self.add_tag(f"level_{level}")

    def set_required(self, b):
        if b:
            self.add_tag("compulsory")
        else:
            self.remove_tag("compulsory")

    def set_image(self, path):
        path = os.path.join(self._asset_path, path)
        assert os.path.isfile(path)

        with open(path, "rb") as image_file:
            self._image_b64 = base64.b64encode(image_file.read()).decode("ascii")

    def _xml_question(self):
        attributes = self._attributes | {"type": self._type}
        return ET.Element("question", attributes)

    def _xml_name(self):
        name = ET.Element("name")
        ET.SubElement(name, "text").text = self.name
        return name

    def _xml_question_text(self):
        # Génération du texte au format HTML
        if '<div' not in self.text:
            html = f'<div>{self.text}</div>'
        else:
            html = self.text

        html = self._alter_question_html(html)

        # Création de la balise
        attributes = {"format": "html"}
        question_text = ET.Element("questiontext", attributes)
        ET.SubElement(question_text, "text").text = html

        return question_text

    def _xml_append_image(self, parent):
        if not self._image_b64:
            return
        ET.SubElement(ET.SubElement(parent, "image_base64"), "text").text = self._image_b64

    def _alter_question_html(self, html):
        html = format_div_tags(html)
        html = format_code_tags(html)
        html = format_span_tags(html)
        return html

    def _xml_append_metas(self, parent):
        ET.SubElement(parent, "externallink")
        ET.SubElement(parent, "usecase").text = "1" if self._case_sensitive else "0"
        ET.SubElement(parent, "defaultgrade").text = str(self.grade)
        ET.SubElement(parent, "editeur").text = "0"

    def _xml_append_custom_metas(self, parent):
        pass

    def _xml_append_answers(self, parent):
        for answer in self.answers:
            parent.append(self._xml_answer(answer))

    def _xml_answer(self, answer):
        raise NotImplementedError

    def _xml_append_tags(self, parent):
        tags = ET.SubElement(parent, "tags")
        for t in self._tags:
            tag = ET.SubElement(tags, "tag")
            ET.SubElement(tag, "text").text = t

    def to_xml(self):
        question = self._xml_question()
        question.append(self._xml_name())
        question.append(self._xml_question_text())
        self._xml_append_image(question)
        self._xml_append_metas(question)
        self._xml_append_custom_metas(question)
        self._xml_append_answers(question)
        self._xml_append_tags(question)
        return question


class SingleChoiceQuestion(Question):
    def __init__(self, data=None, asset_path="."):
        Question.__init__(self, data, asset_path)
        self._type = "multichoice"
        self._fraction = 100
        self._answer_format = "plain_text"

        if self.grade < 1:
            self.grade = 1

        if data and "answer_format" in data:
            self.set_answer_format(data["answer_format"])

        if data and "answers" in data:
            self._init_answers(data)

    def _init_answers(self, data):
        for answer in data["answers"]:
            answer = str(answer)
            validation = "(OK)" in answer
            value = answer.replace("(OK)", "").strip()
            self.add_answer(value, validation)

    def set_answer_format(self, format):
        self._answer_format = format

    @property
    def answers(self):
        self._calculate_fraction()
        return [(a[0], self._fraction if a[1] else 0, a[2]) for a in self._answers]

    def estimated_time(self):
        return 5 + len(self._answers) * 5

    def _calculate_fraction(self):
        right_answers_count = 0
        for answer in self._answers:
            right_answers_count += 1 if answer[1] else 0
        assert right_answers_count == 1, self.name

    def _xml_append_custom_metas(self, parent):
        ET.SubElement(parent, "single").text = "true"

    def _xml_answer(self, answer):
        # Texte de la réponse
        if self._answer_format == 'code':
            attributes = {"fraction": str(answer[1]), "format": "html"}
            e = ET.Element("answer", attributes)

            attributes = {"style": "font-family: 'courier new', courier;"}
            span = ET.Element("span", attributes)
            span.text = answer[0]
            html = ET.tostring(span).decode("utf-8")

            ET.SubElement(e, "text").text = html
        else:
            attributes = {"fraction": str(answer[1]), "format": "plain_text"}
            e = ET.Element("answer", attributes)
            ET.SubElement(e, "text").text = answer[0]

        # Écriture d'un commentaire dans le cadre de la solution
        ET.SubElement(ET.SubElement(e, "feedback"), "text").text = answer[2]
        return e


class MultipleChoiceQuestion(SingleChoiceQuestion):
    def __init__(self, data=None, asset_path="."):
        SingleChoiceQuestion.__init__(self, data, asset_path)

        if self.grade < 2:
            self.grade = 2

    def estimated_time(self):
        return 5 + len(self._answers) * 5

    def _calculate_fraction(self):
        right_answers_count = 0
        for answer in self._answers:
            if answer[1]:
                right_answers_count += 1

        assert right_answers_count > 1
        self._fraction = 100 // right_answers_count

    def _xml_append_custom_metas(self, parent):
        ET.SubElement(parent, "single").text = "false"

    def _alter_question_html(self, html):
        html = SingleChoiceQuestion._alter_question_html(self, html)
        html += '<div style="font-family: Arial; font-size: 13px;"><br /><em><span style="color: #843fa1;">(plusieurs choix possibles)</span></em></div>'
        return html


class MatchingQuestion(Question):
    def __init__(self, data=None, asset_path="."):
        Question.__init__(self, data, asset_path)
        self._type = "matching"

        if self.grade < 2:
            self.grade = 2

        if "answers" in data:
            self._init_answers(data)

    def estimated_time(self):
        return 5 + len(self._answers) * 5

    def _init_answers(self, data):
        for answer in data["answers"]:
            value, validation = [data.strip() for data in answer.split("|")]
            self.add_answer(value, validation)

    def _xml_append_custom_metas(self, parent):
        ET.SubElement(parent, "shuffleanswers").text = "true"

    def _xml_answer(self, answer):
        e = ET.Element("subquestion")
        ET.SubElement(e, "text").text = answer[0]
        ET.SubElement(ET.SubElement(e, "answer"), "text").text = answer[1]
        return e


class ShortAnswerQuestion(Question):
    def __init__(self, data=None, asset_path="."):
        Question.__init__(self, data, asset_path)
        self._type = "shortanswer"
        self._fraction = 100
        self._answer_format = "plain_text"
        self._case_sensitive = False

        if self.grade < 1:
            self.grade = 1

        if data and "answers" in data:
            self._init_answers(data)

    def _init_answers(self, data):
        for value in data["answers"]:
            self.add_answer(value)

    def add_answer(self, value, validation=True, feedback=""):
        self._answers.append((value, validation, feedback))

    @property
    def answers(self):
        return [(a[0], self._fraction, a[2]) for a in self._answers]

    def estimated_time(self):
        return 30

    def _xml_answer(self, answer):
        attributes = {"fraction": str(answer[1]), "format": "plain_text"}
        e = ET.Element("answer", attributes)
        ET.SubElement(e, "text").text = answer[0]

        # Écriture d'un commentaire dans le cadre de la solution
        ET.SubElement(ET.SubElement(e, "feedback"), "text").text = answer[2]
        return e


class ClozeQuestion(Question):
    def __init__(self, data=None, asset_path="."):
        Question.__init__(self, data, asset_path)
        self._type = "cloze"
        self._attributes = {"desc": "variable"}
        self.title = self._value(data, "title", "")
        self.grade = len(self._extract_placeholders(self.text))

    def _xml_question_text(self):
        html = f'<div style="font-family:Arial;font-size:13px;">'
        html += f'<strong>{self.title}</strong><br/>'
        html += '<em><span style="color: #843fa1;">(compléter le texte à trous ci-dessous)</span></em>'
        html += '<br/><br/></div>'
        html += f'<div style="font-family:Arial;font-size:13px;">{self.text}</div>'

        html = self._alter_question_html(html)

        # Création de la balise
        attributes = {"format": "html"}
        question_text = ET.Element("questiontext", attributes)
        ET.SubElement(question_text, "text").text = html

        return question_text

    def _extract_placeholders(self, text):
        start = 0
        placeholders = []

        # Extract parts
        while True:
            try:
                begin = text.index("{", start)
                end = text.index("}", begin)
                placeholders.append(text[begin:end + 1])
                start = end
            except ValueError:
                break

        return placeholders

    def _alter_question_html(self, text):
        # Mise au format Cloze
        # Référence : https://docs.moodle.org/401/en/Embedded_Answers_(Cloze)_question_type
        placeholders = self._extract_placeholders(text)
        replace = {}

        # Mise au format pour chaque placeholder
        for p in placeholders:
            choices = [c.strip() for c in p[1:-1].split("|")]
            cloze_choices = []

            for choice in choices:
                if "(OK)" in choice:
                    choice = choice.replace("(OK)", "").strip()
                    cloze_choices.append(f"%100%{choice}#")
                else:
                    cloze_choices.append(f"%0%{choice}#")

            cloze_choices = "~".join(cloze_choices)
            cloze = "{:MULTICHOICE:%s}" % cloze_choices
            replace[p] = cloze

        # Remplacement des placeholders par la version cloze
        for origin, target in replace.items():
            text = text.replace(origin, target)

        return text

    def estimated_time(self):
        return self.grade * 10


_TYPES = {
    "single": SingleChoiceQuestion,
    "multiple": MultipleChoiceQuestion,
    "match": MatchingQuestion,
    "short": ShortAnswerQuestion,
    "cloze": ClozeQuestion,
}
