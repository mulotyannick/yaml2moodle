import argparse
from quiz import Quiz
from pathlib import Path


def run():
    # Lecture des arguments de la ligne de commande.
    parser = argparse.ArgumentParser()
    parser.add_argument('input', type=argparse.FileType('r'))
    parser.add_argument('output', nargs='?', type=argparse.FileType('wb'))
    args = parser.parse_args()

    if not args.output:
        p = Path(args.input.name)
        output = p.with_suffix('.xml').open('wb')
    else:
        output = args.output

    quiz = Quiz.from_yaml(args.input)
    quiz.print_stats()
    quiz.write(output)


if __name__ == "__main__":
    run()
