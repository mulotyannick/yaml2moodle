from html import escape

def format_div_tags(text: str) -> str:
    return text.replace("<div>", '<div style="font-family:Arial;font-size:13px;">')


def format_span_tags(text: str) -> str:
    return text.replace('class="code"', 'style="font-family:\'courier new\',courier;font-size:13px;"')


def format_code_tags(text: str) -> str:
    if not "<code>" in text:
        return text

    lines = text.split("\n")
    start = None
    i = 0

    while i < len(lines):
        line = lines[i]

        if "<code>" in line:
            start = i
        if "</code>" in line:
            code = _lines_to_code(lines[start:i + 1])
            lines = lines[:start] + [code] + lines[i + 1:]
            i = 0

        i += 1

    return "\n".join(lines)


def _lines_to_code(lines):
    output = "<div>&nbsp;</div>"

    for line in lines:
        line = line.replace("<code>", "").replace("</code>", "")

        if line.strip() != "":
            line = escape(line)
            line = line.replace(" ", "&nbsp;")
            line = '<div style="font-family:\'courier new\',courier;font-size:13px;">' + line + "</div>"
            output += line

    output += "<div>&nbsp;</div>"
    return output
